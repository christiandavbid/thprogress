CHANGE CONTROL
..................................................................................
El prop�sito de la pr�ctica de control de cambios es maximizar la cantidad de cambios exitosos de TI al garantizar que los riesgos se hayan evaluado adecuadamente, autorizar los cambios para que procedan y administrar el programa de cambios".




RELEASE MANAGEMENT
................................................................................................
 El prop�sito de la pr�ctica de gesti�n de lanzamientos es hacer nuevos y Servicios modificados y caracter�sticas disponibles para su uso ".


IT ASSET MANAGEMENT
................................................................................................
El prop�sito de la pr�ctica de gesti�n de activos de TI es planificar y administrar el ciclo de vida completo de todos los activos de TI


DEPLOYMENT MANAGEMENT 
................................................................................................
El prop�sito de la pr�ctica de administraci�n de la implementaci�n es mover hardware, software, documentaci�n, procesos o cualquier otro componente nuevo o modificado a entornos activos


SERVICE DESK -> 
................................................................................................
Los mostradores de servicios proporcionan una ruta clara para que los usuarios informen problemas, consultas, y solicite, y haga que sean reconocidos, clasificados, de propiedad y en acci�n

El prop�sito de la pr�ctica de la mesa de servicio es capturar la demanda de resoluci�n de incidentes y solicitudes de servicio. Tambi�n debe ser el punto de entrada y el �nico punto de contacto para el proveedor de servicios con todos sus usuarios


INCIDENT MANAGEMENT -> 
................................................................................................
La pr�ctica de "gesti�n de incidentes" se ocupa solo de incidentes, no Consultas y peticiones. �El prop�sito de la pr�ctica de manejo de incidentes es minimizar el impacto negativo de los incidentes mediante la restauraci�n del funcionamiento normal del servicio como lo mas rapido posible

El prop�sito de la pr�ctica de manejo de incidentes es minimizar el impacto negativo de los incidentes al restaurar la operaci�n normal del servicio lo m�s r�pido posible �. La administraci�n de incidentes no proporciona un �nico punto de contacto para los usuarios del servicio.


SERVICE LEVEL MANAGEMENT -> 
................................................................................................
Los objetivos del servicio de aseguramiento de la pr�ctica de 'gesti�n de nivel de servicio' Met No gestiona las consultas y peticiones de los usuarios. "El prop�sito de la pr�ctica de la gesti�n del nivel de servicio es establecer objetivos de servicio claros basados en el negociorendimiento, de modo que la prestaci�n de un servicio pueda ser adecuadamente evaluada, monitoreada,y manejado contra estos objetivos

La gesti�n de nivel de servicio identifica m�tricas y medidas que son un reflejo veraz de la experiencia real del cliente y el nivel de satisfacci�n con todo el servicio ", y" Se necesita compromiso para comprender y confirmar las necesidades y los requisitos actuales de los clientes, no simplemente lo que se interpreta por el proveedor del servicio o ha sido acordado varios a�os antes


CONTINUAL IMPROVEMENT -> 
................................................................................................
El prop�sito de la pr�ctica de mejora continua es alinear las pr�cticas y servicios de la organizaci�n con las cambiantes necesidades comerciales a trav�s de la mejora continua de los productos, servicios y pr�cticas, o cualquier elemento involucrado en la administraci�n de productos y servicios "

Aunque todos deber�an contribuir de alguna manera, al menos deber�a haber un peque�o equipo dedicado a tiempo completo para liderar los esfuerzos de mejora continua y promover la pr�ctica en toda la organizaci�n ".

� �Diferentes tipos de mejoras pueden requerir diferentes m�todos de mejora. Por ejemplo, algunas mejoras pueden organizarse mejor en un proyecto de varias fases, mientras que otras pueden ser m�s apropiadas como un solo esfuerzo r�pido ".

"La pr�ctica de mejora continua es parte integral del desarrollo y mantenimiento de todas las dem�s pr�cticas".

"Cuando los proveedores externos forman parte del panorama de servicios, tambi�n deben ser parte del esfuerzo de mejora".

usar un nuevo m�todo cada vez es inapropiado

Es una buena idea seleccionar algunos m�todos clave que sean apropiados para los tipos de mejora que la organizaci�n generalmente maneja y cultivar esos m�todos

La gu�a describe c�mo hay muchos m�todos que se pueden usar para iniciativas de mejora y advierte sobre el uso de demasiados.



HELP DESK -> 
................................................................................................
El prop�sito de la pr�ctica de la mesa de servicio es capturar la demanda de resoluci�n de incidentes y solicitudes de servicio. Tambi�n debe ser el punto de entrada y el �nico punto de contacto para el proveedor de servicios con todos sus usuarios
"Otro aspecto clave de una buena mesa de servicio es su comprensi�n pr�ctica de la organizaci�n m�s amplia, los procesos de negocios y los usuarios".

Las actividades de gesti�n de problemas pueden identificar oportunidades de mejora en las cuatro dimensiones de la gesti�n de servicios. En algunos casos, las soluciones pueden tratarse como oportunidades de mejora, por lo que se incluyen en un registro de mejora continua (CIR), y se utilizan t�cnicas de mejora continua para priorizarlas y gestionarlas ".

El control de errores tambi�n incluye la identificaci�n de posibles soluciones permanentes que pueden resultar en una solicitud de cambio para la implementaci�n de una soluci�n

PROBLEM MANAGEMENT -> 
................................................................................................
El prop�sito de la pr�ctica de gesti�n de problemas es reducir la probabilidad y el impacto de los incidentes mediante la identificaci�n de las causas reales y potenciales de los incidentes, y la gesti�n de soluciones y errores conocidos ".

Una soluci�n efectiva de incidentes puede convertirse en una forma permanente de resolver algunos problemas cuando resolver el problema no es viable ni rentable. En este caso, el problema permanece en el estado de error conocido y se aplica la soluci�n documentada en caso de que se produzcan incidentes relacionados.